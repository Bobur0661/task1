package uz.pdp.task5.generators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import uz.pdp.task5.entity.*;
import uz.pdp.task5.payload.SalaryDto;
import uz.pdp.task5.payload.SalaryPaidDto;
import uz.pdp.task5.payload.TaskDto;
import uz.pdp.task5.payload.UserDto;
import uz.pdp.task5.repo.*;

import java.util.HashSet;
import java.util.List;

@Service
public class Generator {
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    RoleRepo roleRepo;
    @Autowired
    UserRepo userRepo;

    @Autowired
    TaskRepo taskRepo;

    @Autowired
    TaskStateRepo taskStateRepo;

    @Autowired
    EmployeeSalaryRepo employeeSalaryRepo;

    @Autowired
    PaidSalaryRepo paidSalaryRepo;

    public User userDtoToUser(UserDto userDto) {
        User user = new User();
        if (userDto.getId() != null)
            user = userRepo.getById(userDto.getId());
        user.setId(userDto.getId());
        user.setName(userDto.getName());
        user.setLastName(userDto.getLastName());
        user.setUsername(userDto.getUsername());
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        user.setEmail(userDto.getEmail());
        user.setRoles(roleRepo.getById(userDto.getRoleId()));
        return user;
    }

    public UserDto userTOUserDto(User user) {
        UserDto userdto = new UserDto();
        userdto.setId(user.getId());
        userdto.setUsername(user.getUsername());
        userdto.setEmail(user.getEmail());
        userdto.setLastName(user.getLastName());
        return userdto;
    }


    public Task taskDtoToTask(TaskDto taskDto, User user) {

        Task task = new Task();
        if (taskDto.getId() != null)
            task = taskRepo.getById(taskDto.getId());

        task.setId(taskDto.getId());
        task.setName(taskDto.getName());
        task.setUserOwner(user);
        task.setExecutorUser(userRepo.getById(taskDto.getExecutorUser().getId()));
        task.setDescription(taskDto.getDescription());
        task.setDone(taskDto.isDone());
        task.setEndTime(taskDto.getEndTime());
        task.setState(taskStateRepo.getById(taskDto.getState().getId()));

        return task;

    }

    public TaskDto getTaskDtoFromTask(Task task) {
        return new TaskDto(
                task.getId(),
                userTOUserDto(task.getUserOwner()),
                userTOUserDto(task.getExecutorUser()),
                task.getDescription(),
                task.getEndTime(),
                task.getName(),
                task.getState(),
                task.isDone()

        );
    }

    public EmployeeSalary dtoToSalary(SalaryDto salaryDto) {

        EmployeeSalary employeeSalary = new EmployeeSalary();
        if (salaryDto.getId() != null)
            employeeSalary = employeeSalaryRepo.getById(salaryDto.getId());

        employeeSalary.setUser(userRepo.getById(salaryDto.getUser().getId()));
        employeeSalary.setSalary(salaryDto.getSalary());
        employeeSalary.setPayingMonthDate(salaryDto.getPayingMonthDate());
        employeeSalary.setActive(salaryDto.isActive());

        return employeeSalary;
    }

    public PaidSalary dtoToSalaryPaid(SalaryPaidDto salaryPaidDto) {

        PaidSalary paidSalary = new PaidSalary();

        if (salaryPaidDto.getId() != null)
            paidSalary = paidSalaryRepo.getById(salaryPaidDto.getId());

        paidSalary.setUser(userRepo.getById(salaryPaidDto.getUserDto().getId()));
        paidSalary.setAmount(salaryPaidDto.getAmount());
        return paidSalary;
    }

    public SalaryPaidDto paidSalaryToDto(PaidSalary paidSalary){
       return new SalaryPaidDto(
                paidSalary.getId(),
                userTOUserDto(paidSalary.getUser()),
                paidSalary.getAmount(),
                paidSalary.getMonth()
        );
    }
}
