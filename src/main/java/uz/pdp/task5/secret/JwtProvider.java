package uz.pdp.task5.secret;

import io.jsonwebtoken.*;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import uz.pdp.task5.entity.User;
import uz.pdp.task5.repo.UserRepo;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Component
public class JwtProvider {
    @Value("${jwt.secretKey}")
    private String key;
    @Value("${jwt.expireDateInMilliSecund}")
    private Long expireDateMiliSecund;

    @Autowired
    UserRepo userRepo;

    public String generateToken(User user) {
        Date issueDate = new Date();
        Date expireDate = new Date(issueDate.getTime() + expireDateMiliSecund);

        return Jwts
                .builder()
                .setSubject(user.getId().toString())
                .setIssuedAt(issueDate)
                .setExpiration(expireDate)
                .signWith(SignatureAlgorithm.HS512, key)
                .claim("roles", user.getRoles())
                .compact();
    }

    public boolean validateToken(String token) {
        try {
            Jwts
                    .parser()
                    .setSigningKey(key)
                    .parseClaimsJws(token);
            return true;
        } catch (
                ExpiredJwtException e) {
            System.err.println("Muddati o'tgan");
            Date expiration = Jwts.parser()
                    .setSigningKey(key)
                    .parseClaimsJws(token)
                    .getBody()
                    .getExpiration();

        } catch (
                MalformedJwtException malformedJwtException) {
            System.err.println("Buzilgan token");
        } catch (
                SignatureException s) {
            System.err.println("Kalit so'z xato");
        } catch (
                UnsupportedJwtException unsupportedJwtException) {
            System.err.println("Qo'llanilmagan token");
        } catch (IllegalArgumentException ex) {
            System.err.println("Bo'sh token");
        }
        return false;
    }

    public User getUserFromToken(String token) {
        String userId = Jwts.parser()
                .setSigningKey(key)
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
        final Optional<User> userOptional = userRepo.findById(UUID.fromString(userId));
        return userOptional.orElseGet(null);
    }


}

