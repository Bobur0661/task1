package uz.pdp.task5.entity.enums;

public enum RoleNames {
    ROLE_DIRECTOR,
    ROLE_HR_MANAGER,
    ROLE_EMPLOYEE
}
