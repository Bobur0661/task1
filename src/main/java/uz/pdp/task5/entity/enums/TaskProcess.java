package uz.pdp.task5.entity.enums;

public enum TaskProcess {
    NEW,
    PROCESS,
    DONE
}
