package uz.pdp.task5.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import uz.pdp.task5.entity.template.AbsUUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Size;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class EmployeeSalary extends AbsUUID {

    @ManyToOne
    private User user;

    private double salary;


    @Column(nullable = false)
    @Size(max = 31)
    private Integer payingMonthDate;

    private boolean active;



}
