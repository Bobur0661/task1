package uz.pdp.task5.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import uz.pdp.task5.entity.enums.RoleNames;
import uz.pdp.task5.entity.template.AbsUUID;

import javax.persistence.*;
import java.util.UUID;

@NoArgsConstructor
@Data
@Entity
public class Role   implements GrantedAuthority {
    @Id
    @GeneratedValue
    private UUID id;

    @Enumerated(value = EnumType.STRING)
    @Column(unique = true,nullable = false)
    private RoleNames role;

    public Role(RoleNames role) {
        this.role = role;
    }

    @Override
    public String getAuthority() {
        return role.name();
    }
}
