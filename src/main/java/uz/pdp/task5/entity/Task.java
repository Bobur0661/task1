package uz.pdp.task5.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import uz.pdp.task5.entity.enums.TaskProcess;
import uz.pdp.task5.entity.template.AbsEntity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Task extends AbsEntity {

    @ManyToOne(optional = false)
    private User UserOwner;

    @ManyToOne(optional = false)
    private User executorUser;

    @Type(type = "text")
    private String description;

    @Column(nullable = false)
    private Timestamp endTime;

    @ManyToOne(optional = false)
    private TaskState state;


    private boolean done;

}
