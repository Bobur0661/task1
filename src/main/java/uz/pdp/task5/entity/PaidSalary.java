package uz.pdp.task5.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.task5.entity.template.AbsUUID;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class PaidSalary extends AbsUUID {

    @ManyToOne
    private User user;

    private double amount;

    private Integer month;
}
