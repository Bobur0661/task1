package uz.pdp.task5.entity.template;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.Objects;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@MappedSuperclass
public class AbsEntity extends AbsUUID {

    @Column(nullable = false)
    private String name;


    public AbsEntity(UUID id,  String name) {
        super(id);
        this.name = name;
    }
}
