package uz.pdp.task5.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import uz.pdp.task5.entity.enums.TaskProcess;
import uz.pdp.task5.entity.template.AbsUUID;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class TaskState extends AbsUUID {

    @Enumerated(value = EnumType.STRING)
    private TaskProcess name;



}
