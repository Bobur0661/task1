package uz.pdp.task5.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalaryPaidDto {

    private UUID id;

    private UserDto userDto;

    private double amount;
    private Integer month;
}
