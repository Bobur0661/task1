package uz.pdp.task5.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SiginInDto {
    @NotNull
    private String emailOrUsername;
    @Size(min = 6)
    private String password;
}
