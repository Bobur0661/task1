package uz.pdp.task5.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ApiResponse {
    private boolean success;
    private String message;
    private Object obj;
    private long totalElement;

    public ApiResponse(boolean success, String message) {
        this.success = success;
        this.message = message;
    }


    public ApiResponse(boolean success, String message, Object obj) {
        this.success = success;
        this.message = message;
        this.obj = obj;
    }
}

