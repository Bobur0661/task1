package uz.pdp.task5.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.task5.entity.TaskState;

import java.sql.Timestamp;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskDto {

    private UUID id;
    private UserDto OwnerUser;
    private UserDto executorUser;
    private String description;
    private Timestamp endTime;
    private String name;
    private TaskState state;
    private boolean done;
}
