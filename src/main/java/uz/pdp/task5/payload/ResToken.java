package uz.pdp.task5.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class ResToken {
    private String typeToken = "Bearer ";
    private String accessToken;

    public ResToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
