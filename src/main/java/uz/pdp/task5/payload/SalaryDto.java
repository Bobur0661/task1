package uz.pdp.task5.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.pdp.task5.entity.User;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalaryDto {

    private UUID id;

    private UserDto user;

    private double salary;

    private Integer payingMonthDate;
    private boolean active;
}
