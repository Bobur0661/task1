package uz.pdp.task5.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CheckTaskStateDto {
    private UUID taskId;
    private UUID stateId;
    private Boolean done;
}
