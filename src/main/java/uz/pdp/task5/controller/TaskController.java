package uz.pdp.task5.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task5.entity.User;
import uz.pdp.task5.payload.ApiResponse;
import uz.pdp.task5.payload.CheckTaskStateDto;
import uz.pdp.task5.payload.TaskDto;
import uz.pdp.task5.secret.CurrentUser;
import uz.pdp.task5.servise.TaskServise;

import java.util.UUID;

@RestController
@RequestMapping("/api/task")
public class TaskController {

    @Autowired
    TaskServise taskServise;


    /**
     * for examle
     * // "id":
     * "executorUser":
     * {
     * "id":"6ab8972b-3e8b-4859-85d9-c892f6f2eab1"
     * },
     * "description":"bu vazifatni tezroq qiling",
     * "endTime":"2022-03-09",
     * "name":"ilhom",
     * "state":
     * {
     * "id":"6d48c56b-b3ff-441c-9981-66cf9ed74adb"
     * },
     * "done":false
     *
     * @return apiresponse
     */
    @PostMapping("/addOrEdit")
    public HttpEntity<?> addOrEdit(@RequestBody TaskDto taskDto, @CurrentUser User user) {

        ApiResponse apiResponse = taskServise.addOrEdit(taskDto, user);

        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    /**
     * Shu bilan birga xodimlarning tizimdagi oynasida ushbu vazifalar ko’rinishi kerak.
     */
    @GetMapping("/mytask")
    public HttpEntity<?> myTask(@CurrentUser User user) {
        ApiResponse apiResponse = taskServise.myTask(user);

        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }


    /**
     * Xodim vazfani bajarib bo’lgach bajarilganligi haqida belgi qo’yadi va ushbu vazifa bajarilganligi haqida vazifa qoygan manager yoki direktorning emailiga xabar boradi
     *
     * for examle
     * {
     * "taskId":"15cdcc70-8f6a-4f22-8fea-1a3c751a01bb",
     * "stateId":"6227bb10-69b9-4cdb-8242-051f81ad4694"
     * }
     *
     * Togatish
     * {
     *     "taskId":"9e0d319d-77db-4bed-9f85-372d9755272f",
     *   "done":true
     * }
     */
    @PostMapping("/checkTaskStateOrDone")
    public HttpEntity<?> checkTaskState(@CurrentUser User user, @RequestBody CheckTaskStateDto checkTaskStateDto) {
        ApiResponse apiResponse = taskServise.checkTaskState(user, checkTaskStateDto);

        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }




    @GetMapping("/notDone/{userId}")
    public HttpEntity<?>unfulfilled(@CurrentUser User user, @PathVariable UUID userId){
        ApiResponse apiResponse = taskServise.notDone(user, userId);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);

    }


    //Rahbariyat xodimlarga berilgan vazifalarni o’z vaqtida bajargani yoki o’z vaqtida bajara olmayotgani xaqida malumotlarni ko’ra olishi kerak.
    @GetMapping("/bossOwnerTask")
    public HttpEntity<?>ownerTasks(@CurrentUser User user){
        ApiResponse apiResponse = taskServise.ownerUser(user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }
}
