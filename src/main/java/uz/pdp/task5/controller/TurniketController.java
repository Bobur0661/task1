package uz.pdp.task5.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.task5.entity.User;
import uz.pdp.task5.payload.ApiResponse;
import uz.pdp.task5.payload.TurniketDto;
import uz.pdp.task5.secret.CurrentUser;
import uz.pdp.task5.servise.TurniketService;

@RestController
@Api(value = "saddasd")
@RequestMapping("/turniket")
public class TurniketController {

    @Autowired
    TurniketService turnpiketService;


    /**
     *Turniketning qaysi tomonidan scanner qilinganiga korxonaga kirgan yoki chiqqan vaqt haqida malumotlar qayd etib borilishi kerak.
     *  {
     *     "inOrOut":false
     * }

     */
    @ApiOperation(value = "kirish va chiqish")
    @PostMapping("/inOrOut")
    @ApiParam(hidden = true)
    public HttpEntity<?> inOrOut(@RequestBody TurniketDto turniketDto, @CurrentUser User user) {
        ApiResponse apiResponse = turnpiketService.inOrOut(turniketDto, user);
        return ResponseEntity.ok(apiResponse);
    }



}
