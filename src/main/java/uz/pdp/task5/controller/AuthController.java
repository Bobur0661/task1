package uz.pdp.task5.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.task5.payload.ResToken;
import uz.pdp.task5.payload.SiginInDto;
import uz.pdp.task5.servise.UserRervise;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    UserRervise userRervise;

    @PostMapping("/login")
    public HttpEntity<?> login(@RequestBody SiginInDto siginInDto) {
        ResToken token = userRervise.login(siginInDto);
        return ResponseEntity.ok(token);
    }
}
