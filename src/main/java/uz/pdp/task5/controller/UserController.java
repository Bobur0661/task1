package uz.pdp.task5.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task5.entity.User;
import uz.pdp.task5.payload.ApiResponse;
import uz.pdp.task5.payload.UserDto;
import uz.pdp.task5.repo.UserRepo;
import uz.pdp.task5.secret.CurrentUser;
import uz.pdp.task5.servise.ManagerServise;

@RestController
@RequestMapping("/api/manager")
public class UserController {

    @Autowired
    ManagerServise managerServise;

    @Autowired
    UserRepo userRepo;

    /**
     * add ROLE_HR_MANAGER or ROLE_EMPLOYEE for only admin
     * or hr_manage only add employee
     *
     *{
     * "name":"ilhom",
     * "username":"ilhomjon",
     * "lastName":"mannonov",
     * "password":"1234567",
     * "email":"aa@gmail.com",
     * "roleId":"6e08b9bc-d4f3-4003-a66c-d2d0b45b9425"
     *}
     * @return
     */
    @PostMapping("/addOrEdit")
    public HttpEntity<?> add( @RequestBody UserDto userDto, @CurrentUser User user) {
        ApiResponse apiResponse = managerServise.addOrEdit(userDto, user);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }





}
