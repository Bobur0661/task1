package uz.pdp.task5.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task5.entity.User;
import uz.pdp.task5.payload.ApiResponse;
import uz.pdp.task5.payload.SalaryDto;
import uz.pdp.task5.payload.SalaryPaidDto;
import uz.pdp.task5.secret.CurrentUser;
import uz.pdp.task5.servise.SalaryServise;

import java.util.UUID;

@RestController
@RequestMapping("/salary")
public class EmployeeSalaryController {
    @Autowired
    SalaryServise salaryServise;

    /**
     *
     Xar bir xodimga ma’lum bir oylik ish haqi belgilanadi.
     */
    @PostMapping("/addOrEditSalary")
    public HttpEntity<?> addOrEditSalary(@CurrentUser User user, @RequestBody SalaryDto salaryDto) {

        ApiResponse apiResponse = salaryServise.addOrEditSalary(user, salaryDto);
        return ResponseEntity.ok(apiResponse);
    }

    /**
     * Xodimga oylik berilgach sistemaga qayd etib boriladi.
     */
    @PostMapping("/salaryPaid")
    public HttpEntity<?>salaryPaid(@RequestBody SalaryPaidDto salaryPaidDto){
        ApiResponse apiResponse = salaryServise.paid(salaryPaidDto);
        return ResponseEntity.ok(apiResponse);
    }
//Xodim bo’yicha yoki belgilagan oy bo’yicha berilgan oyliklarni ko’rish imkoniyati bo’lishi kerak.
    @PostMapping("/get")
    HttpEntity<?>getPaidSalaryHistory(@RequestBody SalaryPaidDto salaryPaidDto){
        ApiResponse apiResponse = salaryServise.getPaidSalary(salaryPaidDto);
        return ResponseEntity.ok(apiResponse);
    }


    @DeleteMapping("/delete/{id}")
    public HttpEntity<?>delete(@PathVariable UUID id){
        ApiResponse apiResponse = salaryServise.delete(id);
        return ResponseEntity.ok(apiResponse);
    }
}
