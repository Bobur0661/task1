package uz.pdp.task5.exceptions;

public class PageSizeException extends Exception{
    public PageSizeException(String messge) {
        super(messge);
    }
}
