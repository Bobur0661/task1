package uz.pdp.task5.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.task5.entity.TurniKet;

import java.util.UUID;

public interface TurniKetRepo extends JpaRepository<TurniKet, UUID> {
}
