package uz.pdp.task5.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.task5.entity.Role;

import java.util.Set;
import java.util.UUID;

public interface RoleRepo extends JpaRepository<Role, UUID>{

}
