package uz.pdp.task5.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.task5.entity.User;


import java.util.UUID;


public interface UserRepo extends JpaRepository<User, UUID> {

    User findByUsernameOrEmail(String username, String email);
}
