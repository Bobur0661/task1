package uz.pdp.task5.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.task5.entity.TaskState;

import java.util.UUID;

//@RepositoryRestResource(path = "taskState")
public interface TaskStateRepo extends JpaRepository<TaskState, UUID> {
}
