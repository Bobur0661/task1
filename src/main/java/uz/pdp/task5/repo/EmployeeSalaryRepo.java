package uz.pdp.task5.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.task5.entity.EmployeeSalary;

import java.util.UUID;

public interface EmployeeSalaryRepo extends JpaRepository<EmployeeSalary, UUID> {
}
