package uz.pdp.task5.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import uz.pdp.task5.entity.Task;
import uz.pdp.task5.entity.User;

import java.util.List;
import java.util.UUID;

public interface TaskRepo extends JpaRepository<Task, UUID> {

    @Query(value = "select * from task t where t.user_owner_id=:id and done=false",nativeQuery = true)
    List<Task> findByUserOwner(@Param("id") UUID id);



    List<Task>findByExecutorUser(User executorUser);
    Task findByExecutorUserAndId(User executorUser, UUID id);


    @Query(value = "select * from task t where t.user_owner_id=:userOwner_id and t.executor_user_id=:executorUser_id", nativeQuery = true)
    List<Task> findByExecutorUserAndUserOwner(UUID userOwner_id,UUID executorUser_id);
}
