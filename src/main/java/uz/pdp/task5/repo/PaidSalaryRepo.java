package uz.pdp.task5.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.task5.entity.PaidSalary;

import java.util.List;
import java.util.UUID;

public interface PaidSalaryRepo extends JpaRepository<PaidSalary, UUID> {
    List<PaidSalary>findByUserId(UUID user_id);
    List<PaidSalary> findByMonth(Integer month);
}
