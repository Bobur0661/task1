package uz.pdp.task5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.mail.javamail.JavaMailSender;

@SpringBootApplication
public class Task5Application {

    @Autowired
    JavaMailSender javaMailSender;


    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(Task5Application.class, args);
    }
}


