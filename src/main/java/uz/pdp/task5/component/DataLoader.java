package uz.pdp.task5.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.pdp.task5.Task5Application;
import uz.pdp.task5.entity.Role;
import uz.pdp.task5.entity.TaskState;
import uz.pdp.task5.entity.User;
import uz.pdp.task5.entity.enums.RoleNames;
import uz.pdp.task5.entity.enums.TaskProcess;
import uz.pdp.task5.repo.RoleRepo;
import uz.pdp.task5.repo.TaskStateRepo;
import uz.pdp.task5.repo.UserRepo;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Component
public class DataLoader implements CommandLineRunner {
    @Autowired
    UserRepo userRepo;
    @Autowired
    RoleRepo roleRepo;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    TaskStateRepo taskStateRepo;

    @Override
    public void run(String... args) throws Exception {


//        System.out.println("salom teshavoy");
//        Role director = roleRepo.save(new Role(RoleNames.ROLE_DIRECTOR));
//        roleRepo.save(new Role(RoleNames.ROLE_HR_MANAGER));
//        roleRepo.save(new Role(RoleNames.ROLE_EMPLOYEE));
//
//        taskStateRepo.save(new TaskState(TaskProcess.NEW));
//        taskStateRepo.save(new TaskState(TaskProcess.DONE));
//        taskStateRepo.save(new TaskState(TaskProcess.PROCESS));
//
//        userRepo.save(new User(null, "Admin",
//                "adminaka",
//                "admin",
//                passwordEncoder.encode("123456"),
//                "admin@gmain.com",
//                new HashSet<>(Arrays.asList(director))
//        ));


    }
}
