package uz.pdp.task5.servise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task5.entity.Role;
import uz.pdp.task5.entity.User;
import uz.pdp.task5.entity.enums.RoleNames;
import uz.pdp.task5.generators.Generator;
import uz.pdp.task5.payload.ApiResponse;
import uz.pdp.task5.payload.UserDto;
import uz.pdp.task5.repo.UserRepo;

import javax.mail.search.SearchTerm;
import java.util.Optional;
import java.util.Set;

@Service
public class ManagerServise {

    @Autowired
    UserRepo userRepo;

    @Autowired
    Generator generator;

    public ApiResponse addOrEdit(UserDto userDto, User currentUser) {

        User newUser = generator.userDtoToUser(userDto);

        if (userDto.getId() != null) {

            Optional<User> optionalUser = userRepo.findById(userDto.getId());

            if (optionalUser.isEmpty())
                return new ApiResponse(false, "not fined Manager");
        }


        if (currentUser.getRoles().equals(RoleNames.ROLE_DIRECTOR.name())
                && newUser.getRoles().equals(RoleNames.ROLE_HR_MANAGER.name())) {
            userRepo.save(newUser);
            return new ApiResponse(true, "save manager");
        }

        if (currentUser.getRoles().equals(RoleNames.ROLE_HR_MANAGER.name())
                &&newUser.getRoles().equals(RoleNames.ROLE_EMPLOYEE.name())
        ) {
            userRepo.save(newUser);
            return new ApiResponse(true, "save Employee");
        }


        return new ApiResponse(false, userDto.getId() == null ? "Not Saved" : "Not Edited");
    }

    public String rolename(Set<Role> roles) {
        for (Role role : roles) {
            return role.getRole().name();
        }
        return null;
    }


}
