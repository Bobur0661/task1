package uz.pdp.task5.servise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task5.entity.Task;
import uz.pdp.task5.entity.TaskState;
import uz.pdp.task5.entity.User;
import uz.pdp.task5.entity.enums.RoleNames;
import uz.pdp.task5.generators.Generator;
import uz.pdp.task5.payload.ApiResponse;
import uz.pdp.task5.payload.CheckTaskStateDto;
import uz.pdp.task5.payload.TaskDto;
import uz.pdp.task5.repo.TaskRepo;
import uz.pdp.task5.repo.TaskStateRepo;
import uz.pdp.task5.repo.UserRepo;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class TaskServise {
    @Autowired
    TaskRepo taskRepo;
    @Autowired
    UserRepo userRepo;
    @Autowired
    TaskStateRepo taskStateRepo;
    @Autowired
    Generator generator;

    @Autowired
    ManagerServise managerServise;

    @Autowired
    MailServise mailServise;

    public ApiResponse addOrEdit(TaskDto taskDto, User user) {
        ApiResponse apiResponse = new ApiResponse();
        if (user.getRoles().equals(RoleNames.ROLE_DIRECTOR.name())
                && userRepo.getById(taskDto.getExecutorUser().getId()).getRoles().equals(RoleNames.ROLE_DIRECTOR.name())
        ) {
            apiResponse = new ApiResponse(false, "you dont add task director");
        } else if (user.getRoles().equals(RoleNames.ROLE_HR_MANAGER.name()) &&
                !userRepo.getById(taskDto.getExecutorUser().getId()).getRoles().equals(RoleNames.ROLE_EMPLOYEE.name())
        ) {
            apiResponse = new ApiResponse(false, "you add only employee task");
        } else {

            Task task = generator.taskDtoToTask(taskDto, user);
            taskRepo.save(task);
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    mailServise.sendMessage(task.getExecutorUser().getEmail(), "task", "Sizga " + task.getUserOwner().getName() + " dan yangi topshiriq keldi");
                }
            });
            thread.start();
            apiResponse = new ApiResponse(true, taskDto.getId() == null ? "added" : "edited");
        }
        return apiResponse;

    }

    public ApiResponse myTask(User user) {
        List<TaskDto> byExecutorUser = taskRepo.findByExecutorUser(user)
                .stream()
                .map(item -> generator.getTaskDtoFromTask(item))
                .collect(Collectors.toList());
        return new ApiResponse(true, "ok", byExecutorUser);
    }

    public ApiResponse checkTaskState(User user, CheckTaskStateDto checkTaskStateDto) {
        Task task = taskRepo.findByExecutorUserAndId(user, checkTaskStateDto.getTaskId());
        if (checkTaskStateDto.getDone() != null) {
            task.setDone(checkTaskStateDto.getDone());
            taskRepo.save(task);
        } else {

            TaskState state = taskStateRepo.getById(checkTaskStateDto.getStateId());
            task.setState(state);
            taskRepo.save(task);
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    mailServise.sendMessage(task.getUserOwner().getEmail(), "task", "Hello " + task.getUserOwner().getName() + " !  \n " +
                            task.getExecutorUser().getName() + " vazifa sini bajardi  \n " + "holat: " + state.getName());
                }
            });
            thread.start();
        }

        return new ApiResponse(true, checkTaskStateDto.getDone() != null ? "task changed" : "task done");
    }

    public ApiResponse notDone(User user, UUID userId) {
        List<TaskDto> taskDtoList = taskRepo.findByExecutorUserAndUserOwner(user.getId(), userId)
                .stream()
                .map(item -> generator.getTaskDtoFromTask(item))
                .collect(Collectors.toList());
        return new ApiResponse(true, "ok", taskDtoList);
    }

    public ApiResponse ownerUser(User user) {

        List<TaskDto> taskDtoList = taskRepo.findByUserOwner(user.getId())
                .stream().map(item -> generator.getTaskDtoFromTask(item))
                .collect(Collectors.toList());

        return new ApiResponse(true, "ok",taskDtoList);
    }

}
