package uz.pdp.task5.servise;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import uz.pdp.task5.entity.EmployeeSalary;
import uz.pdp.task5.entity.PaidSalary;
import uz.pdp.task5.entity.User;
import uz.pdp.task5.generators.Generator;
import uz.pdp.task5.payload.ApiResponse;
import uz.pdp.task5.payload.SalaryDto;
import uz.pdp.task5.payload.SalaryPaidDto;
import uz.pdp.task5.repo.EmployeeSalaryRepo;
import uz.pdp.task5.repo.PaidSalaryRepo;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class SalaryServise {
    @Autowired
    EmployeeSalaryRepo employeeSalaryRepo;

    @Autowired
    Generator generator;

    @Autowired
    PaidSalaryRepo paidSalaryRepo;

    public ApiResponse addOrEditSalary(User user, SalaryDto salaryDto) {

        if (user.getId().equals(UUID.randomUUID()))
            return new ApiResponse(false, "o'zingizga maosh nelgilay olmaysiz");


        EmployeeSalary employeeSalary = generator.dtoToSalary(salaryDto);

        employeeSalaryRepo.save(employeeSalary);

        return new ApiResponse(true, salaryDto.getId() != null ? "Edited" : "saved");
    }

    public ApiResponse delete(UUID id) {
        try {
            employeeSalaryRepo.deleteById(id);
            return new ApiResponse(false, "Deleted");

        } catch (Exception e) {
            return new ApiResponse(false, "not Deleted");
        }
    }

    public ApiResponse paid(SalaryPaidDto salaryPaidDto) {

        PaidSalary paidSalary = generator.dtoToSalaryPaid(salaryPaidDto);

        paidSalaryRepo.save(paidSalary);

        return new ApiResponse(true, salaryPaidDto.getId() != null ? "Edited" : "Saved");

    }

    public ApiResponse getPaidSalary(SalaryPaidDto salaryPaidDto) {

        List<SalaryPaidDto> salaryPaidDtoList = new ArrayList<>();

        if (salaryPaidDto.getUserDto().getId() != null) {

           salaryPaidDtoList  = paidSalaryRepo.findByUserId(salaryPaidDto.getUserDto().getId())
                    .stream()
                    .map(item -> generator.paidSalaryToDto(item))
                    .collect(Collectors.toList());


        } else if (salaryPaidDto.getMonth() != null) {
            salaryPaidDtoList = paidSalaryRepo.findByMonth(salaryPaidDto.getMonth())
                    .stream()
                    .map(item->generator.paidSalaryToDto(item))
                    .collect(Collectors.toList());

        }

        return new ApiResponse(true, "ok",salaryPaidDtoList);
    }
}
