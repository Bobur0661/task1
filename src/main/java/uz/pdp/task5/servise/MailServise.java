package uz.pdp.task5.servise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class MailServise {
    @Autowired
    JavaMailSender sender;
    public void sendMessage(String email, String sub, String body) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(email);
        simpleMailMessage.setText(body);
        simpleMailMessage.setSubject(sub);
        sender.send(simpleMailMessage);

    }

}
