package uz.pdp.task5.servise;

import io.jsonwebtoken.Jwt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import uz.pdp.task5.entity.User;
import uz.pdp.task5.payload.ApiResponse;
import uz.pdp.task5.payload.ResToken;
import uz.pdp.task5.payload.SiginInDto;
import uz.pdp.task5.secret.JwtProvider;

@Service
public class UserRervise {

    @Autowired
    JwtProvider jwtProvider;

    @Autowired
    AuthenticationManager manager;

    public ResToken login(SiginInDto siginInDto) {
        Authentication authenticate = manager.authenticate(new UsernamePasswordAuthenticationToken(
                siginInDto.getEmailOrUsername(),
                siginInDto.getPassword()));

        User user = (User) authenticate.getPrincipal();
        return new ResToken(jwtProvider.generateToken(user));

    }


}
