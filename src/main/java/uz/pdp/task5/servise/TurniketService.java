package uz.pdp.task5.servise;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.task5.entity.TurniKet;
import uz.pdp.task5.entity.User;
import uz.pdp.task5.generators.Generator;
import uz.pdp.task5.payload.ApiResponse;
import uz.pdp.task5.payload.TurniketDto;
import uz.pdp.task5.repo.TurniKetRepo;

@Service
public class TurniketService {
    @Autowired
    Generator generator;

    @Autowired
    TurniKetRepo turniKetRepo;
    public ApiResponse inOrOut(TurniketDto turniketDto, User user) {

        TurniKet turniKet = new TurniKet();
        turniKet.setInOrOut(turniketDto.isInOrOut());
        turniKet.setUser(user);
        turniKetRepo.save(turniKet);
        return new ApiResponse(true,turniKet.isInOrOut()?"Ichkariga kirdi":"tashqariga chiqdi");

    }
}

